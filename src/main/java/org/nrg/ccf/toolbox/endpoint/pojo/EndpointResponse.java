package org.nrg.ccf.toolbox.endpoint.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EndpointResponse {
	
	private int error;  
	private String message;  

}
