package org.nrg.ccf.toolbox.endpoint.conf;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@Slf4j
@XnatPlugin(
			value = "nihToolboxEndpointPlugin",
			name = "NIH Toolbox Endpoint Plugin"
		)
@ComponentScan({ 
		"org.nrg.ccf.toolbox.endpoint.conf",
		"org.nrg.ccf.toolbox.endpoint.components",
		"org.nrg.ccf.toolbox.endpoint.xapi"
	})
public class NihToolboxEndpointPlugin {
	
	public NihToolboxEndpointPlugin() {
		log.info("Configuring the NIH Toolbox Endpoint Plugin.");
	}
	
}
