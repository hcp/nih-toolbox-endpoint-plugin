package org.nrg.ccf.toolbox.endpoint.components;

import org.nrg.ccf.common.utilities.abst.AbstractProjectPreferenceBean;
import org.nrg.ccf.common.utilities.components.PreferenceUtils;
import org.nrg.ccf.toolbox.endpoint.pojo.ToolboxEndpointSettings;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.services.NrgPreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value="NihToolboxEndpointProjectSettings")
@NrgPreferenceBean(toolId = "nihToolboxEndpoint", toolName = "NIH Toolbox Endpoint Settings")
public class ProjectSettings extends AbstractProjectPreferenceBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8334074184353636530L;

	@Autowired
	protected ProjectSettings(NrgPreferenceService preferenceService, PreferenceUtils preferenceUtils) {
		super(preferenceService, preferenceUtils);
	}
	
	@NrgPreference
	public Boolean getToolboxEndpointEnabled() {
		return false;
	}

	public Boolean getToolboxEndpointEnabled(final String entityId) {
		return this.getBooleanValue(SCOPE, entityId, "toolboxEndpointEnabled");
	}

	public void setToolboxEndpointEnabled(final String entityId, final Boolean toolboxEndpointEnabled) {
		try {
			this.setBooleanValue(SCOPE, entityId, toolboxEndpointEnabled, "toolboxEndpointEnabled");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getToolboxEndpointResource() {
		return "";
	}
	
	public String getToolboxEndpointResource(final String entityId) {
		return this.getValue(SCOPE, entityId, "toolboxEndpointResource");
	}

	public void setToolboxEndpointResource(final String entityId, final String toolboxEndpointResource) {
		try {
			this.set(SCOPE, entityId, toolboxEndpointResource, "toolboxEndpointResource");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}
	
	@NrgPreference
	public Boolean getToolboxEndpointLockResource() {
		return false;
	}

	public Boolean getToolboxEndpointLockResource(final String entityId) {
		return this.getBooleanValue(SCOPE, entityId, "toolboxEndpointLockResource");
	}

	public void setToolboxEndpointLockResource(final String entityId, final Boolean toolboxEndpointLockResource) {
		try {
			this.setBooleanValue(SCOPE, entityId, toolboxEndpointLockResource, "toolboxEndpointLockResource");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	public ToolboxEndpointSettings getToolboxEndpointSettings(String projectId) {
		final ToolboxEndpointSettings settings = new ToolboxEndpointSettings();
		settings.setToolboxEndpointEnabled(this.getToolboxEndpointEnabled(projectId));
		settings.setToolboxEndpointResource(this.getToolboxEndpointResource(projectId));
		settings.setToolboxEndpointLockResource(this.getToolboxEndpointLockResource(projectId));
		return settings;
	}

	public static ToolboxEndpointSettings getDefaultSettings() {
		return ToolboxEndpointSettings.getDefaultToolboxEndpointSettings();
	}

}
