package org.nrg.ccf.toolbox.endpoint.xapi;


import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.common.utilities.constants.LockOperation;
import org.nrg.ccf.common.utilities.utils.CcfWorkflowUtils;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.toolbox.endpoint.components.ProjectSettings;
import org.nrg.ccf.toolbox.endpoint.constants.EndpointConstants;
import org.nrg.ccf.toolbox.endpoint.pojo.EndpointResponse;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnat.services.archive.CatalogService.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@XapiRestController
@Api(description = "NIH Toolbox Endpoint API")
public class NihToolboxEndpointApi extends AbstractXapiRestController {
	
	
	
	private CatalogService _catalogService;
	private ProjectSettings _projectSettings;
	private String WARNING_MESSAGE = "WARNING:  One or more files could not be uploaded (uploader supports only CSV files).";
	
	@Autowired
	@Lazy
	public NihToolboxEndpointApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, CatalogService catalogService,
			ProjectSettings projectSettings) {
		super(userManagementService, roleHolder);
		_catalogService = catalogService;
		_projectSettings = projectSettings;
	}
	
	@ApiOperation(value = "Endpoint NIH Toolbox data", 
			notes = "Endpoint for NIH Toolbox data",
			response = EndpointResponse.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/toolboxEndpoint/project/{projectId}"}, restrictTo=AccessLevel.Member, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<EndpointResponse> uploadFiles(@PathVariable("projectId") @ProjectId final String projectId, 
    		@ApiParam(name = "upload", required=false) @RequestPart(name = "upload", required=false) final MultipartFile upload, 
    		@ApiParam(name = "upload[]", required=false) @RequestPart(name="upload[]", required=false) final MultipartFile[] uploadArr, final HttpServletRequest request) throws NrgServiceException {

		final XnatProjectdata project = XnatProjectdata.getXnatProjectdatasById(projectId, getSessionUser(), false);
		if (project == null) {
			return new ResponseEntity<EndpointResponse>(new EndpointResponse(2, "Project is not found (ProjectID=" + projectId + ")"),HttpStatus.NOT_FOUND);
		}
		if (!_projectSettings.getToolboxEndpointEnabled(projectId)) {
			return new ResponseEntity<EndpointResponse>(new EndpointResponse(4, "Toolbox endpoint is not enabled for this project (ProjectID=" + projectId + ")"),HttpStatus.NOT_FOUND);
		}
		//final Map<String, MultipartFile> fileMap = upload.getFileMap();
		if (upload == null && (uploadArr == null || uploadArr.length<1)) {
			return new ResponseEntity<EndpointResponse>(new EndpointResponse(1, "No files were found on this request. Nothing done."),HttpStatus.OK);
		}
		final XnatResourcecatalog resource;
		try {
			resource = ResourceUtils.getOrCreateResource(project, _projectSettings.getToolboxEndpointResource(projectId), getSessionUser());
		} catch (ServerException | ClientException e) {
			throw new NrgServiceException("Could not create or obtain resource", e);
		}
		ResourceUtils.resourceLocker(resource, LockOperation.UNLOCK);
		int uploadCount=0;
		String warningMessage = "";
		if (upload != null) {
			if (writeFileToArchive(upload, resource, request)) { 
				uploadCount+=1; 
			} else {
				warningMessage = WARNING_MESSAGE;
			};
		}
		if (uploadArr != null) {
			for (final MultipartFile mpFile : Arrays.asList(uploadArr)) {
				if (writeFileToArchive(mpFile, resource, request)) {
					uploadCount+=1;
				} else {
					warningMessage = WARNING_MESSAGE;
				};
			}
		}
		try {
			final String resourceStr = ResourceUtils.getResourceSb(resource);
			_catalogService.refreshResourceCatalog(getSessionUser(), resourceStr, Operation.ALL) ;
		} catch (ServerException | ClientException e) {
			throw new NrgServiceException("Failed to refresh endpoint catalog", e);
		}
		try {
			CcfWorkflowUtils.generateStatusCompleteWorkflow(getSessionUser(), project.getItem(), EventUtils.CATEGORY.DATA, EventUtils.TYPE.REST, EndpointConstants.TOOLBOX_ENDPOINT_SAVE_WORKFLOW);
		} catch (Exception e) {
			log.error("Failed to generate endpoint upload workflow entry", e);
		}
		if (_projectSettings.getToolboxEndpointLockResource(projectId)) {
			ResourceUtils.resourceLocker(resource, LockOperation.LOCK);
		}
		return new ResponseEntity<EndpointResponse>(new EndpointResponse(0,"Successfully uploaded " + uploadCount + " files.  " + warningMessage),HttpStatus.OK);
    }
	
	@ApiOperation(value = "Unlock toolbox endpoint folder", 
			notes = "Unlock toolbox endpoint folder",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/toolboxEndpoint/project/{projectId}/unlockEndpointResource"}, restrictTo=AccessLevel.Admin, method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> unlockResource(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {

		final XnatProjectdata project = XnatProjectdata.getXnatProjectdatasById(projectId, getSessionUser(), false);
		if (project == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		final XnatResourcecatalog resource;
		try {
			resource = ResourceUtils.getOrCreateResource(project, _projectSettings.getToolboxEndpointResource(projectId), getSessionUser());
		} catch (ServerException | ClientException e) {
			throw new NrgServiceException("Could not create or obtain resource", e);
		}
		if (resource == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			ResourceUtils.resourceLocker(resource, LockOperation.UNLOCK);
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
    }
	
	@ApiOperation(value = "Lock toolbox endpoint folder", 
			notes = "Unlock toolbox endpoint folder",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/toolboxEndpoint/project/{projectId}/lockEndpointResource"}, restrictTo=AccessLevel.Admin, method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> lockResource(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {

		final XnatProjectdata project = XnatProjectdata.getXnatProjectdatasById(projectId, getSessionUser(), false);
		if (project == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		final XnatResourcecatalog resource;
		try {
			resource = ResourceUtils.getOrCreateResource(project, _projectSettings.getToolboxEndpointResource(projectId), getSessionUser());
		} catch (ServerException | ClientException e) {
			throw new NrgServiceException("Could not create or obtain resource", e);
		}
		if (resource == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			ResourceUtils.resourceLocker(resource, LockOperation.LOCK);
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
    }

	private boolean writeFileToArchive(MultipartFile upload, XnatResourcecatalog resource, HttpServletRequest request) throws NrgServiceException {
		final File resourcePath = ResourceUtils.getResourcePathFile(resource);
		final StringBuilder sb = new StringBuilder(upload.getOriginalFilename());
		if (!sb.toString().toUpperCase().endsWith(".CSV")) {
			return false;
		}
		final String formattedCurrentDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.now());
		sb.append("_");
		sb.append(request.getRemoteAddr());
		sb.append("_");
		sb.append(formattedCurrentDate);
		sb.append("_");
		sb.append(getSessionUser().getLogin());
		final File resourceFile = new File(resourcePath,sb.toString());
		try {
			FileUtils.writeByteArrayToFile(resourceFile, upload.getBytes());
			return true;
		} catch (IOException e) {
			throw new NrgServiceException("Failed to write endpoint file", e);
		}
	}
	
}
