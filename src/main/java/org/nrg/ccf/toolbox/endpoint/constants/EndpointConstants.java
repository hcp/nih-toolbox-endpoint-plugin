package org.nrg.ccf.toolbox.endpoint.constants;

public class EndpointConstants {
	
	public static final String TOOLBOX_ENDPOINT_SAVE_WORKFLOW = "Imported NIH Toolbox Files to Endpoint";
	public static final String DEFAULT_ENDPOINT_RESOURCE = "toolbox_endpoint_data";

}
