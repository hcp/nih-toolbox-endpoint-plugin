package org.nrg.ccf.toolbox.endpoint.pojo;

import org.nrg.ccf.toolbox.endpoint.constants.EndpointConstants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ToolboxEndpointSettings {
	
	private Boolean toolboxEndpointEnabled; 
	private String toolboxEndpointResource; 
	private Boolean toolboxEndpointLockResource; 
	
	public static ToolboxEndpointSettings getDefaultToolboxEndpointSettings() {
		return new ToolboxEndpointSettings(false, EndpointConstants.DEFAULT_ENDPOINT_RESOURCE, false);
	}
	
}
