package org.nrg.ccf.toolbox.endpoint.xapi;

import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.common.utilities.constants.LockOperation;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.toolbox.endpoint.components.ProjectSettings;
import org.nrg.ccf.toolbox.endpoint.pojo.ToolboxEndpointSettings;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Lazy
@XapiRestController
@Api(description = "NIH Toolbox Endpoint Config API")
public class NihToolboxEndpointConfigApi extends AbstractXapiRestController {

	private ProjectSettings _projectSettings;

	@Autowired
	@Lazy
	public NihToolboxEndpointConfigApi(UserManagementServiceI userManagementService, RoleHolder roleHolder,
			ProjectSettings projectSettings) {
		super(userManagementService, roleHolder);
		_projectSettings = projectSettings;
	}
	
	@ApiOperation(value = "Gets project structural qc settings", notes = "Returns NDA structural qc plugin project-level settings.",
			response = ToolboxEndpointSettings.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/toolboxEndpointConfig/{projectId}/settings"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ToolboxEndpointSettings> getToolboxEndpointSettings(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		
		ToolboxEndpointSettings toolboxEndpointSettings = _projectSettings.getToolboxEndpointSettings(projectId); 
		if (toolboxEndpointSettings == null) {
			toolboxEndpointSettings = ProjectSettings.getDefaultSettings();
		} 
		return new ResponseEntity<ToolboxEndpointSettings>(toolboxEndpointSettings,HttpStatus.OK);
		
    }
	
	@ApiOperation(value = "Sets project transfer settings", notes = "Sets project-level transfer settings",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/toolboxEndpointConfig/{projectId}/settings"}, restrictTo=AccessLevel.Owner, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setToolboxEndpointSettings(@PathVariable("projectId") @ProjectId final String projectId, @RequestBody final ToolboxEndpointSettings settings) throws NrgServiceException {
		_projectSettings.setToolboxEndpointEnabled(projectId, settings.getToolboxEndpointEnabled());
		_projectSettings.setToolboxEndpointResource(projectId, settings.getToolboxEndpointResource());
		_projectSettings.setToolboxEndpointLockResource(projectId, settings.getToolboxEndpointLockResource());
		// Lock or unlock resource based on settings.
		final XnatProjectdata project = XnatProjectdata.getXnatProjectdatasById(projectId, getSessionUser(), false);
		if (project == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		XnatResourcecatalog resource;
		try {
			resource = ResourceUtils.getOrCreateResource(project, _projectSettings.getToolboxEndpointResource(projectId), getSessionUser());
			if (resource != null) {
				ResourceUtils.resourceLocker(resource, (settings.getToolboxEndpointLockResource()) ? LockOperation.LOCK : LockOperation.UNLOCK );
			}
		} catch (ServerException | ClientException e) {
			// Do nothing
		}
		return new ResponseEntity<>(HttpStatus.OK);
    }
	
}
